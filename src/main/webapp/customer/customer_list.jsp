<%@page import="com.customer.demo.model.Customer"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>List of Books</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<style>
body {
	background-image: url("image.jpg");
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
}
</style>
</head>
<body>
	<!-- header -->
	<nav class="navbar navbar-dark justify-content-between">
		<h1 class="btn btn-outline-success my-2 my-sm-0">List of Customer</h1>
		<form class="form-inline">
			<button class="btn btn-outline-success my-2 my-sm-0">
				<a style="text-decoration: none;" class="text-success"
					href="add-customer" id="">Add New Customer</a>
			</button>
		</form>
	</nav>
	<nav class="navbar navbar-dark bg-success justify-content-between">
		<h3
			class="my-2 my-sm-0 text-center offset-sm-4 offset-md-4 offset-lg-4">Customer
			Management System</h3>
	</nav>
	<!-- main container  -->
	<div class='container'>
		<div class="row">
			<table class="table table-success mt-3">
				<h2 class="offset-sm-3 offset-md-3 offset-lg-3 mt-2 text-center text-danger">${message}</h2>
				<h2 class="offset-sm-3 offset-md-3 offset-lg-3 mt-2 text-center text-success">${msg2}</h2>
				<thead>
					<tr>
						<th scope="col">Customer Id</th>
						<th scope="col">First Name</th>
						<th scope="col">Last Email</th>
						<th scope="col">Email</th>
						<th scope="col" colspan="2" class="pl-5">Action</th>

					</tr>
				</thead>
				<tbody class="text-success">
					<%
					List<Customer> customers = (List<Customer>) request.getAttribute("customers");
					if (!customers.isEmpty()) {
						for (Customer customer : customers) {
					%>
					<tr>
						<td><p><%=customer.getCustomerId()%></p></td>
						<td><p><%=customer.getFirstName()%></p></td>
						<td><p><%=customer.getLastName()%></p></td>
						<td><p><%=customer.getEmail()%></p></td>
						<td><a
							href='update-customer?customerId=<%=customer.getCustomerId()%>'><i
								class="fa fa-edit text-warning"
								style="font-size: 22px; text-decoration: none;"></i></a></td>
						<td><a
							href='delete-customer?customerId=<%=customer.getCustomerId()%>'><i
								class="fa fa-trash text-danger"
								style="font-size: 22px; text-decoration: none;"></i></a></td>

					</tr>
					<%
					}
					}
					%>

				</tbody>
			</table>
		</div>
	</div>
</body>
</html>