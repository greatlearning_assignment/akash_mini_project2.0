<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>User Edit</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<style>
body {
	background-image: url("image.jpg");
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
}
</style>
</head>
<body>
	<!-- header -->
	<nav class="navbar navbar-dark bg-success justify-content-between">
	</nav>
	<nav class="navbar navbar-dark bg-success justify-content-between">
		<h3 class="my-2 my-sm-0 text-center offset-sm-4 offset-md-4 offset-lg-4">Customer Management System</h3>
	</nav>
	<!-- main container  -->
	<div class='container pt-5 pb-5'>
		<div class="row">
			<div class="col-lg-3"></div>
			<div class="col-lg-6 col-lg-offset-3">
				<form action="add-customer" method="post"
					class="bg-success pt-5 pb-5 pl-5 pr-5 border border-primary ">
				    <%
						String msg = (String) request.getAttribute("error");
						if (msg != null) {
					%>
					<h4 class='text-center text-warning'><%=msg%></h4>
					<%
						}
					%>
				    
					<h3 class='text-center'>Please Enter Customer Details</h3>
					<div class="form-group pt-5">
						<label for="fisrtName"><h4>First Name</h4></label><input type="text"
							class="form-control" id="firstName" value="" placeholder="Enter first name"
							name="firstName">
					</div>
					<div class="form-group">
						<label for="lastName"><h4>Last Name</h4></label> <input type="text"
							class="form-control" id="lastName" value=""  placeholder="Enter last name"
							name="lastName">
					</div>
					<div class="form-group">
						<label for="email"><h4>Email</h4></label> <input
							type="text" class="form-control" id="email"  value="" name="email" placeholder="Enter email address">
					</div>
					<div align="center">
						<button type="submit"
							class="btn btn-outline-success my-2 my-sm-0 bg-danger" name="add-customer"
							value="add-customer">Add Customer</button>
					</div>
				</form>
			</div>
			<div class="col-lg-3"></div>
		</div>
	</div>
</body>
</html>