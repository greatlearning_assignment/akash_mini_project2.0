package com.customer.demo.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.customer.demo.model.Customer;
import com.customer.demo.service.CustomerService;

@Controller
@RequestMapping("/customer")
public class CustomerController {
	@Autowired
	private CustomerService customerService;

	@GetMapping("/customer-list")
	public String getLogin(@RequestParam(required = false) String message, Map<String, List<Customer>> map,
			Map<String, String> msg2, Map<String, String> msg) {
		List<Customer> customers = this.customerService.getCustomerList();
		if (!customers.isEmpty()) {
			map.put("customers", customers);
			msg2.put("msg2", message);
			return "customer_list";
		} else {
			map.put("customers", customers);
			msg.put("message", "Customer List is empty");
			return "customer_list";
		}

	}

	@GetMapping("/add-customer")
	public String addCustomer() {
		return "add_customer";
	}

	@PostMapping("/add-customer")
	public String insertCustomer(@RequestParam(required = false) String customerId, Customer customer) {
		if (this.customerService.insertCustomer(customer)) {
			return "redirect:customer-list?message=New Customer is added successfully";
		} else {
			return "redirect:customer-list?message=Customer is already exist";
		}
	}

	@GetMapping("/update-customer")
	public String getCustomer(int customerId, Map<String, Customer> map, HttpSession httpSession) {
		Customer customer = this.customerService.getCustomerById(customerId);
		httpSession.setAttribute("customerId", customerId);

		if (customer != null) {
			map.put("customer", customer);
			return "update_customer";
		} else {
			return "redirect:list?error=Customer is not updated";
		}
	}

	@PostMapping("/update-customer")
	public String updateCustomer(Customer customer, HttpSession httpSession) {
		customer.setCustomerId((int) httpSession.getAttribute("customerId"));
		if (this.customerService.updateCustomer(customer)) {
			return "redirect:customer-list?message=Customer is updated successfully";
		}
		return "redirect:customer-list?message=Customer is not updated";
	}

	@GetMapping("/delete-customer")
	public String deleteUser(@RequestParam int customerId) {
		if (this.customerService.deleteCustomerById(customerId)) {
			return "redirect:customer-list?message=Customer is deleted successfully";
		}
		return "redirect:customer-list?message=Customer is not deleted";
	}
}
