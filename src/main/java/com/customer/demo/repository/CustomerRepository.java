package com.customer.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.customer.demo.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer>{
	
}
