package com.customer.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.customer.demo.model.Customer;
import com.customer.demo.repository.CustomerRepository;

@SpringBootApplication
public class CustomerManagementApplication {

	@Autowired
	private CustomerRepository customerRepository;

	public static void main(String[] args) {
		SpringApplication.run(CustomerManagementApplication.class, args);
	}

	@Bean
	public void insertData() {
		
		Customer cust1 = new Customer("Akash", "Pandey", "akash_p@hcl.com.com");
		Customer cust2 = new Customer("Yogesh", "Pandey","yogesh123@hcl.com");
		Customer cust3 = new Customer("Sonal", "Garde","sonal123@hcl.com");
		Customer cust4 = new Customer("Nikhil", "Pandey", "nikhil123@hcl.com");
		Customer cust5 = new Customer("Faim", "Khan", "faim124@hcl.com");
		Customer cust6 = new Customer("Anjali", "Pandey","anjali124@hcl.xom");

		customerRepository.save(cust1);
		customerRepository.save(cust2);
		customerRepository.save(cust3);
		customerRepository.save(cust4);
		customerRepository.save(cust5);
		customerRepository.save(cust6);
		
	}

}
