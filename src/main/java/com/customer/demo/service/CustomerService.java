package com.customer.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.customer.demo.model.Customer;
import com.customer.demo.repository.CustomerRepository;

@Service
public class CustomerService {
	@Autowired
	private CustomerRepository customerRepository;

	public boolean insertCustomer(Customer customer) {
		if (this.customerRepository.existsById(customer.getCustomerId())) {
			return false;
		}
		this.customerRepository.save(customer);
		return true;
	}

	public List<Customer> getCustomerList() {
		return (List<Customer>) this.customerRepository.findAll();
	}

	public Customer getCustomerById(int customerId) {
		return this.customerRepository.findById(customerId).get();
	}

	public boolean updateCustomer(Customer customer) {
		if (this.customerRepository.existsById(customer.getCustomerId())) {
			this.customerRepository.save(customer);
			return true;
		}
		return false;
	}

	public boolean deleteCustomerById(int customerId) {
		if (!this.customerRepository.existsById(customerId)) {
			return false;
		}
		this.customerRepository.deleteById(customerId);
		return true;
	}

}
